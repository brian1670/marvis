﻿using UnityEngine;
using System.Collections;

public class displayImage : MonoBehaviour {

    // Use this for initialization
//    private displayMap dispMaps;

    IEnumerator Start()
    {
//        dispMaps = GameObject.Find("dMap").GetComponent<displayMap>();

        string imgurl = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Flag_of_Afghanistan_%281880%E2%80%931901%29.svg/2000px-Flag_of_Afghanistan_%281880%E2%80%931901%29.svg.png";

        
        return SetImage(imgurl);
    }

    IEnumerator SetImage(string imgurl)
    {
        WWW www = new WWW(imgurl);
        yield return www;

        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = www.texture;
    }

    IEnumerator changeImage(string imgurl)
    {
        return SetImage(imgurl);
    }

    // Update is called once per frame
    void Update()
    {
        string imgurl = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Flag_of_Afghanistan_%281880%E2%80%931901%29.svg/2000px-Flag_of_Afghanistan_%281880%E2%80%931901%29.svg.png";

        //have to put myo code in here
        if (displayMap.charSelected != "string")
        {

            using (var wc = new System.Net.WebClient())
            {
                string urlstart = "http://gateway.marvel.com/v1/public/characters";
                string urlend = "&ts=1462040307&apikey=44087c7f3f78c8b7a95983f1a1cf452f&hash=0ed555fb0a4cba2df788067bbb93d3bb&limit=10";

                //split character by whitespace
                string[] split = displayMap.charSelected.Split(' ');

                //check if the new split string array has more than one string
                //if so, the character has more than one string value in their name and must have %20

                string newCharName = "";
                if (split.Length > 1)
                {
                    for (int m = 0; m < split.Length; m++)
                    {
                        newCharName += split[m];
                        if (m != split.Length - 1)
                        {
                            newCharName += "%20";
                        }
                    }
                }
                else
                {
                    newCharName = split[0];
                }

                string urlmid = "?name=" + newCharName;

                string url_Character = urlstart + urlmid + urlend;

                //				print ("url " + url_ + "\n");

                var url = wc.DownloadString(url_Character);
                //api pull
                var json_char = SimpleJSON.JSON.Parse(url);

                //IMAGE PULL REQUESTS

                string img_path = json_char["data"]["results"][0]["thumbnail"]["path"];
                string img_extension = json_char["data"]["results"][0]["thumbnail"]["extension"];
                string size = "landscape_incredible";

                imgurl = img_path + "/" + size + "." + img_extension;

            }
        }
        changeImage(imgurl);

    }
}