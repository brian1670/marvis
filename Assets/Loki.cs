﻿using UnityEngine;
using SimpleJSON;
//using System.Collections;
using System;
using System.Collections.Generic;
using System.Collections;

public class Loki : MonoBehaviour
{

    //global variable in back screen
    //public Dictionary<string, List<string>> charInfo;

    //global variable in front screen
    //	public List<string> characters; 
    public string voiceRecognized;


    IEnumerator Start()
    {
        //        mainMaps = GameObject.Find("mMap").GetComponent<mainMap>();
        mainMap.charInfo = new Dictionary<string, List<string>>();

        //		using (var wc = new System.Net.WebClient())
        //		{

        //LETS USE THE MOST POPULAR CHARACTERS (I guess idk)
        //1 Spiderman
        //2 Storm
        //3 Rogue
        //4 Thor
        //5 Iron Man 
        //6 Captain America --> voice recog
        //7 Emma Frost
        //8 Black Widow --> voice recog
        //9 Loki
        //10 Hulk

        voiceRecognized = "christines string";
        //

        //			string urlstart = "http://gateway.marvel.com/v1/public/characters";
        //			string urlend = "&ts=1462040307&apikey=44087c7f3f78c8b7a95983f1a1cf452f&hash=0ed555fb0a4cba2df788067bbb93d3bb&limit=10";

        //map each character to an array info
        //find the ID for each character in the list and map it to an ID
        //add to this dictionary for each new character

        //for initial heroes
        //			foreach(string s in characters) {
        string testString = "Loki";


        //		
        //			}

        return newCharacter(testString);

        //		//test code to see titles IT WORKS!!!


        //

        //end of start function
    }

    //function to get and set info of a character through voice recognition
    //takes a string as a parameter/argument 
    IEnumerator newCharacter(string charName)
    {

        using (var wc = new System.Net.WebClient())
        {
            string urlstart = "http://gateway.marvel.com/v1/public/characters";
            string urlend = "&ts=1462040307&apikey=44087c7f3f78c8b7a95983f1a1cf452f&hash=0ed555fb0a4cba2df788067bbb93d3bb&limit=10";

            //split character by whitespace
            string[] split = charName.Split(' ');

            //check if the new split string array has more than one string
            //if so, the character has more than one string value in their name and must have %20

            string newCharName = "";
            if (split.Length > 1)
            {
                for (int m = 0; m < split.Length; m++)
                {
                    newCharName += split[m];
                    if (m != split.Length - 1)
                    {
                        newCharName += "%20";
                    }
                }
            }
            else
            {
                newCharName = split[0];
            }

            string urlmid = "?name=" + newCharName;

            string url_Character = urlstart + urlmid + urlend;

            //				print ("url " + url_ + "\n");

            var url = wc.DownloadString(url_Character);
            //api pull
            var json_char = SimpleJSON.JSON.Parse(url);
            int id = json_char["data"]["results"][0]["id"].AsInt;

            //string which is description of character
            string descrip = json_char["data"]["results"][0]["description"];

            //				print ("description: "+ descrip + "\n");

            //input that as characters/id#/series?apikey
            //limit the query to 20 
            //back screen will show: 
            //get the ["data"]["results"]["description"](if there is a description)

            //url for getting series
            string urlSERIES = urlstart + "/" + id.ToString() + "/series?" + urlend;

            //				print (urlSERIES);

            var url_S = wc.DownloadString(urlSERIES);

            var json_SERIES = SimpleJSON.JSON.Parse(url_S);

            //number of series available, though we're limiting this to 20 anyways
            int seriesAmt = json_SERIES["data"]["count"].AsInt;

            //check if the description for the given hero is null
            //if it is, use "None"

            if (descrip == null)
            {
                descrip = "None";
            }

            //list to map to each hero
            //first two elements will always be the id followed by description

            List<string> charDescrip = new List<string>() {
                id.ToString (),
                descrip
            };

            //get all titles, add them to the list
            for (int k = 0; k < seriesAmt; k++)
            {
                string title = json_SERIES["data"]["results"][k]["title"];
                charDescrip.Add(title);
            }

            //map the character to its information
            mainMap.charInfo[charName] = charDescrip;


            //add the new character to the list of characters if it doesn't already exist
            //			if(!characters.Contains(charName)){
            //				characters.Add (charName);
            //			}

            //			print (charName + ": ");
            //			//
            //			for(int p = 0; p < charInfo[charName].Count; p++)
            //			{
            //				print (charInfo [charName] [p] + ", ");
            //			}

            //IMAGE PULL REQUESTS

            string img_path = json_char["data"]["results"][0]["thumbnail"]["path"];
            string img_extension = json_char["data"]["results"][0]["thumbnail"]["extension"];
            string size = "landscape_incredible";

            string imgurl = img_path + "/" + size + "." + img_extension;

            return SetImage(imgurl);
        }


    }

    IEnumerator SetImage(string imgurl)
    {

        WWW www = new WWW(imgurl);
        yield return www;

        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = www.texture;
    }
    //
    // Update is called once per frame
    void Update()
    {
        //have to put myo code in here

    }
}

//other test and pseudo code

//create an array for each character that provides the name, description (always the first two elements)
//insert into url with "?name=Iron%20Man&apikey"...
//get the ["data"]["results"][0]["id"] as int?
//input that as characters/id#/series?apikey
//limit the query to 20 
//back screen will show: 
//get the ["data"]["results"]["description"](if there is a description) &&
//take ["data"]["results"]["title"] and add those to an array, just list them 
//with an endline after each title



//test code to print names of first 40 characters indiscriminately. it works!

//			for (int i = 0; i < 40; i++) {
//				character = json["data"]["results"][i]["name"];
//				characters.Add (character);
//			}
////			Console.WriteLine ("Why isn't this value showing up? ");
//
//			for (int k = 0; k < 40; k++) {
//				print (characters [k] + "\n");
//			}
