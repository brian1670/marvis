﻿using UnityEngine;
using SimpleJSON;
//using System.Collections;
using System;
using System.Collections.Generic;
using System.Collections;

public class mainMap : MonoBehaviour {

	//global variable in back screen
	public static Dictionary<string, List<string> > charInfo;

	//global variable in front screen
	public static List<string> characters; 
	public static string voiceRecognized;
	public static string characterSelected;

	// Use this for initialization
	void Start () {

		charInfo = new Dictionary<string, List<string>> ();

		characters = new List<string>()
		{
			"Spider-man", 
			"Storm", 
			"Rogue", 
			"Thor", 
			"Deadpool", 
			"Emma Frost", 
			"Loki", 
			"Hulk"
		};
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
